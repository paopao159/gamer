import React from 'react';
import { StyleSheet, Text, View,Button,Image,TouchableOpacity,Platform } from 'react-native';
import { Actions } from "react-native-router-flux";

export default class Home2 extends React.Component {
  Result4() {
    Actions.result4()
  }
  Home() {
    Actions.home()
  }
  constructor(props){
    super(props)
    this.state = {
      text: "News",
      }
  }
  render() {
    return (
      <View style={styles.wrapper}>
        <View style={styles.box1}>
           <View style={{flexDirection: 'row',}}>
              <Image
                 source={require('../img/pier.jpg')}
                 style={styles.circle0}
              />
              <Text style={styles.textProfile}>
               Mario
              </Text>
            </View>
              <Image 
                 source={require('../img/BW.png')}
                 style={styles.logo}
              />
        </View>
        <View style={styles.box}>
            <Image 
                 source={require('../img/ios.png')}
                 style={styles.ios}
              />
              {/* <TouchableOpacity onPress={this.Result2}> */}
              <View style={styles.button}>
             <TouchableOpacity onPress={this.Result2}>
               <Text style={styles.buttontext}>
               ฉันเข้าใจแล้ว
               </Text>
              </TouchableOpacity>
            </View>
            {/* </TouchableOpacity> */}
        </View>  
              
            
         

       </View> 
    )
  }
}

const styles = StyleSheet.create({
  wrapper: { 
    flex: 1,
    display: 'flex',
    paddingTop: Platform.os === "ios" ? 25 : 0,
    backgroundColor: '#403f3f',
   },
   textProfile: {
    fontSize: 16,
    alignItems: 'flex-start',
    marginTop: 35,
    marginLeft: 10,
    fontWeight: 'bold',
    color: '#FFFFFF',
   },
   logo: {
    width: 35, 
    height: 35, 
    marginTop: 30,
    alignItems: 'flex-end',
   },
   box: {
    backgroundColor: '#4d4f55',
    marginTop: '5%',
    marginLeft: '2%',
    marginRight: '2%',
    flexDirection: 'row',
    justifyContent: "space-between",
    alignItems: 'flex-start',
    height: '75%',
    borderRadius:15, 
   },
   box1: {
    backgroundColor: '#403f3f',
    flexDirection: 'row',
    justifyContent: "space-between",
    alignItems: "stretch",
    // shadowColor:'red',
    height: '12%',
    width: '100%',
    shadowOpacity: 1,
    shadowRadius: 3,
    shadowOffset: {
    height: 0,
    width: 0
    },
   },
   circle0: {
    width: 50,
    height: 50,
    borderRadius: 50/2,
    alignItems: 'flex-start',
    marginTop: 20,
    marginLeft: 10,
    borderWidth: 3,
    borderColor: '#FFFFFF',
   },
   ios: {
    height: '85%',
    borderTopRightRadius:15,
    borderTopLeftRadius:15,
    overflow: "hidden"
    // borderBottomRightRadius: 0,
    // borderBottomLeftRadius: 0,
   },
   button: {
    bottom: 0,
    position: 'absolute',
    marginVertical: 10,
    padding: 15,
    borderRadius:15, 
    width: 260,
    // alignItems:'center',
    backgroundColor: '#403f3f',
    marginHorizontal: '14%'
   },
   buttontext: {
    fontSize: 17,
    width: '100%',
    textAlign: 'center',
    color: '#FFFFFF',
   },
});
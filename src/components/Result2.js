import React from 'react';
import {StyleSheet, Text, View,TouchableHighlight,Platform,TouchableOpacity,ScrollView ,Image} from 'react-native';
import { Actions } from "react-native-router-flux";
// import { Icon } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';


export default class Result2 extends React.Component {

  Result3() {
    Actions.result3()
  }
  Home() {
    Actions.home()
  }
  constructor(props){
    super(props)
    this.state = {
      text: "เลือกทีมที่ชนะ",
      colorBotton:'#a5a8a6',
      circleColorChange:0
    }
  }

  _handlePress = (team) => {
    // e.preventDefault();
    // styles.circleBtnBg.backgroundColor = 'transparent'
    this.setState({
      text: 'ต่อไป',
      colorBotton:'#2b7b4a',
      circleColorChange:team
    })
  }
  

  render() {
    const styles = StyleSheet.create({
      wrapper: { 
        flex: 1,
        paddingTop: Platform.os === "ios" ? 25 : 0,
        backgroundColor: '#cc6600',
       },
       result: {
        flexWrap: 'wrap',
        textAlign: 'center',
        fontSize: 30,
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#FFFFFF',
       },
       tex0:{
         flexWrap: 'wrap',
         textAlign: 'center',
         fontSize: 14,
         fontWeight: '300',
         marginTop: 15,
         textAlign: 'center',
         color: '#FFFFFF',
       },
       tex1:{
         flexWrap: 'wrap',
         textAlign: 'center',
         fontSize: 15,
         fontWeight: '300',
         textAlign: 'center',
         color: '#FFFFFF',
       },
       tex2:{
        marginTop: '100%',
        fontSize: 13,
        fontWeight: '300',
        textAlign: 'center',
        color: '#FFFFFF',
      },
      tex22:{
        marginTop: '20%',
        fontSize: 13,
        fontWeight: '300',
        textAlign: 'center',
        color: '#FFFFFF',
      },
      tex3:{
        textAlign: 'center',
        fontSize: 10,
        color: '#FFFFFF',
      },
       time:{
        marginTop: '20%',
        fontSize: 30,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#FFFFFF',
       },
       container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
      },
       containertex: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
       },
       circle1: {
        width: 100,
        height: 100,
        borderRadius: 100/2,
        alignItems: 'flex-start',
        marginTop: '100%',
        borderWidth: 3,
        borderColor: (this.state.circleColorChange == 1) ? 'green' :'#FFFFFF',
       },
       circle2: {
        width: 100,
        height: 100,
        borderRadius: 100/2,
        alignItems: 'flex-start',
        marginTop: '100%',
        borderWidth: 3,
        borderColor: (this.state.circleColorChange == 2) ? 'green' :'#FFFFFF',
       },
       circle3: {
        width: 15,
        height: 15,
        borderRadius: 15/2,
        alignItems: 'center',
        marginBottom: '15%',
        marginLeft: 12,
        marginRight: 12,
        backgroundColor: '#FFFFFF'
       },
       circle31: {
        width: 15,
        height: 15,
        borderRadius: 15/2,
        alignItems: 'center',
        marginBottom: '15%',
        marginLeft: 12,
        marginRight: 12,
        borderWidth: 2,
        borderColor: '#FFFFFF',
        backgroundColor: '#cc6600'
       },
       button: {
        bottom: 0,
        position: 'absolute',
        marginVertical: 10,
        padding: 15,
        borderRadius:15, 
        width: 260,
        alignSelf:'center',
        backgroundColor: this.state.colorBotton,
       },
       buttontext: {
        fontSize: 17,
        width: '100%',
        textAlign: 'center',
        color: '#FFFFFF',
       },
       icon: {
        alignItems: 'flex-end',
        marginTop: 15,
       },
       circleBtnBg: {
         backgroundColor: 'transparent'
       }
    });
    return (
    <View style={styles.wrapper}>
      <View style={styles.icon}>
        <Icon color="white" name="times-circle" size={30} onPress={this.Home} />
      </View>
      <ScrollView>
          <Text style={styles.result}>
            ส่งผลการแข่งขัน
            </Text>
            <Text style={styles.tex0}>
            ทีมใดเป็นฝ่ายชนะ ?
            </Text>
        <View style={styles.container}>
        <View style={styles.containertex}>
          <TouchableHighlight underlayColor='#cc6600' onPress={()=>this._handlePress.bind(this)(1)} >
             <Image
                 source={require('../img/Virtuspro.png')}
                 style={styles.circle1}
              />
          </TouchableHighlight>
          <Text style={styles.tex22}>
              VirtusPro
          </Text>
        </View>
          <View style={styles.containertex}>
            <Text style={styles.tex2}>
            รอบที่: 1 
            </Text>
            <Text style={styles.tex3}> 
            19 พฤษภาคม 2018 
            </Text>
            <Text style={styles.time}>
            20:00
            </Text>
            </View>
            <View style={styles.containertex}>
            <TouchableHighlight underlayColor='#cc6600' onPress={()=>this._handlePress.bind(this)(2)}>
              <Image
                 source={require('../img/th.png')}
                 style={styles.circle2}
              />
            </TouchableHighlight>
            <Text style={styles.tex22}>
                   Thailand
            </Text>
        </View>
       </View>
       </ScrollView>
        <View style={styles.containertex}>
        <View style={styles.container}>
          <View style={styles.circle31}>
          </View>
          <View style={styles.circle3}>
          </View>
          <View style={styles.circle3}>
          </View>
        </View>
            <View style={styles.button}>
             <TouchableOpacity onPress={this.Result3}>
               <Text style={styles.buttontext}>
               {this.state.text}
               </Text>
              </TouchableOpacity>
            </View>
      </View>
    </View> 
    
    )
  }
}


import React from 'react';
import { StyleSheet, Text,TextInput, View,Button,Image,TouchableOpacity,Platform } from 'react-native';
import { Actions } from "react-native-router-flux";

export default class Home extends React.Component {
  Result() {
    Actions.result()
  }
  Home2() {
    Actions.home2()
  }
  Home() {
    Actions.home()
  }
  constructor(props){
    super(props)
    this.state = {
      text: "News",
      // name: '',
      }
  }
  render() {
    return (
      <View style={styles.wrapper}>
        <View style={styles.box1}>
           <View style={{flexDirection: 'row',}}>
              <Image
                 source={require('../img/pier.jpg')}
                 style={styles.circle0}
              />
              <TouchableOpacity>
              <Text style={styles.textProfile}>              >
               Mario
              </Text>
              </TouchableOpacity>

            </View>
              <Image 
                 source={require('../img/BW.png')}
                 style={styles.logo}
              />
        </View>
          <Text style={styles.mat}>
            แมทช์ของคุณ
          </Text>
          <TouchableOpacity onPress={this.Result}>
          <View style={styles.box}>
            <View style={styles.containertex}>
             <Image
                 source={require('../img/Virtuspro.png')}
                 style={styles.circle1}
              />
                <Text style={styles.tex2}>
                   VirtusPro
                </Text>
              </View>
          <View style={styles.containertex}>
            <Text style={styles.tex2}>
            รอบที่: 1 
            </Text>
            <Text style={styles.tex3}> 
            19 พฤษภาคม 2018 
            </Text>
            <Text style={styles.time}>
            20:00
            </Text>
          </View>
          <View style={styles.containertex}>
            <Image
                 source={require('../img/th.png')}
                 style={styles.circle2}
              />
              <Text style={styles.tex2}>
                   Thailand
              </Text>
          </View>
         </View>
         </TouchableOpacity>

         <View style={styles.button}>
          <View style={styles.rowtext}>
              <View style={styles.rowtext2}>
               <TouchableOpacity onPress={this.Home2}>
                   <Text style={styles.buttontext}>
                     {this.state.text}
                   </Text>
                </TouchableOpacity>
                </View>
                <View style={styles.rowtext2}>
               <TouchableOpacity onPress={this.Home}>
                   <Text style={styles.buttontext2}>
                     My Tournament
                   </Text>
               </TouchableOpacity>
              </View>
            </View>
          </View>

       </View> 
    )
  }
}

const styles = StyleSheet.create({
  wrapper: { 
    flex: 1,
    display: 'flex',
    paddingTop: Platform.os === "ios" ? 25 : 0,
    backgroundColor: '#403f3f',
   },
   textProfile: {
    fontSize: 16,
    // paddingLeft: Platform.os === "ios" ? 40 : 0,
    // paddingTop: Platform.os === "ios" ? 25 : 0,
    alignItems: 'flex-start',
    marginTop: 35,
    marginLeft: 10,
    fontWeight: 'bold',
    color: '#FFFFFF',
   },
   logo: {
    width: 35, 
    height: 35, 
    marginTop: 30,
    alignItems: 'flex-end',
   },
   mat: {
     padding: 10,
     textAlign: 'justify',
     fontSize: 30,
     fontWeight: 'bold',
     color: '#FFFFFF',
   },
   box: {
    backgroundColor: '#2b7b4a',
    margin: 10,
    padding: 20,
    flexDirection: 'row',
    justifyContent: "space-between",
    alignItems: 'flex-start',
    height: 140,
   },
   box1: {
    backgroundColor: '#403f3f',
    flexDirection: 'row',
    justifyContent: "space-between",
    alignItems: "stretch",
    height: '12%',
    width: '100%',
    shadowOpacity: 1,
    shadowRadius: 3,
    shadowOffset: {
    height: 0,
    width: 0
    },
   },
   tex2:{
    flexWrap: 'wrap',
    textAlign: 'center',
    fontSize: 13,
    marginTop: 10,
    fontWeight: '300',
    color: '#FFFFFF',
  },
  tex3:{
    textAlign: 'center',
    fontSize: 10,
    marginTop: 5,
    color: '#FFFFFF',
  },
   time:{
    fontSize: 25,
    marginTop: 30,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#FFFFFF',
   },
   containertex: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
   },
   circle0: {
    width: 50,
    height: 50,
    borderRadius: 50/2,
    alignItems: 'flex-start',
    marginTop: 20,
    marginLeft: 10,
    borderWidth: 3,
    borderColor: '#FFFFFF',
   },
   circle1: {
    width: 80,
    height: 80,
    borderRadius: 80/2,
    alignItems: 'flex-start',
    borderWidth: 3,
    borderColor: '#FFFFFF',
   },
   circle2: {
    width: 80,
    height: 80,
    borderRadius: 80/2,
    alignItems: 'flex-end',
    borderWidth: 3,
    borderColor: '#FFFFFF',
   },
   button: {
    bottom: 0,
    position: 'absolute',
    backgroundColor: '#403f3f',
    flexDirection: 'row',
    alignItems: "stretch",
    height: '10%',
    width: '100%',
    shadowOpacity: 1,
    shadowRadius: 0,
    shadowOffset: {
    height: 0,
    width: 0
    },
   },
   rowtext: {
    flexDirection: 'row',
   },
   rowtext2: {
    flexDirection: 'column',
    height: '100%',
   },
   buttontext: {
    fontSize: 10,
    marginLeft: '35%',
    marginTop: '38%',
    justifyContent: 'center',
    alignItems: 'center',
    color: '#FFFFFF',
   },
   buttontext2: {
    fontSize: 10,
    marginTop: '40%',
    marginLeft: '15%',
    justifyContent: 'center',
    alignItems: 'center',
    color: '#FFFFFF',
   },
});
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableWithoutFeedback,
    TouchableOpacity,
    ImageBackground,
    Dimensions,
    Image
} from 'react-native';
import DismissKeyboard from 'dismissKeyboard';
import Logo from './Logo';
import {Icon} from 'react-native-elements';
import login from './Login';
// import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';

const { width, height } = Dimensions.get("window");

export default class LoginSocial extends Component {

goback() {
    Actions.pop();
}
Login() {
    Actions.login()
  }

    render() {
      return (
        <TouchableWithoutFeedback onPress={()=>{DismissKeyboard()}}>
        <View style={styles.container}>
          <ImageBackground style = {styles.bg}
              source={require('../img/bg.jpg')} 
              blurRadius={15}
              resizeMode="cover" 
            >
            <Image style ={styles.logo}
            source={require('../img/BW.png')}
            />

            <View style={styles.buttonBox}>
            <TouchableOpacity style={styles.buttonF}>
            <Icon style={styles.icon}
            name='facebook-square'
            type='font-awesome'
            color='#ffffff'
                />
              <Text style={styles.buttonTextF}>ล็อกอินด้วย Facebook</Text>
            </TouchableOpacity>
          
          
            <TouchableOpacity style={styles.buttonT}>
              <Icon style={styles.icon}
            name='twitter'
            type='font-awesome'
            color='#ffffff'
                />
              <Text style={styles.buttonTextT}>ล็อกอินด้วย Twitter</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.buttonL}>
          <Image style ={styles.linelogo}
            source={require('../img/icons8-line-filled-50.png')}
         />
          {/* <Icon style={styles.icon}
            name='twitch'
            type='font-awesome'
            color='#ffffff'
                /> */}
              <Text style={styles.buttonTextL}>ล็อกอินด้วย Line</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.buttonG}>
          <Icon style={styles.icon}
            name='google'
            type='font-awesome'
            color='#ffffff'
                />
              <Text style={styles.buttonTextG}>ล็อกอินด้วย Google</Text>
          </TouchableOpacity>
          </View>
            
        <View style={styles.footer}> 
          <TouchableOpacity onPress={this.Login} style={styles.buttonfooter}>
          <Image style ={styles.logoSignup}
            source={require('../img/BW.png')}
         />
         <Text style={styles.signupButton}>ลงทะเบียน และ ลงชื่อเข้าใช้ กับเกมเมอร์ที่นี่!</Text>
         </TouchableOpacity> 
         </View>
         </ImageBackground>
          </View>
          
        
        </TouchableWithoutFeedback>
      );
    }
  }

  const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
      },

    buttonF: {  
      width:'80%',
        backgroundColor:'#4267b2',
        borderRadius:7,
        marginVertical: 8, 
        paddingVertical: 12,
        flexDirection: 'row',
        justifyContent: 'center',
        opacity: 0.6
            },

    buttonT: {  
      width:'80%',
        backgroundColor:'#55acee',
        borderRadius:7,
        marginVertical: 8, 
        paddingVertical: 12,
        flexDirection: 'row',
        justifyContent: 'center',
        opacity: 0.6
            },      
            
    buttonL: {  
      width:'80%',
        backgroundColor:'#01c302',
        borderRadius:7,
        marginVertical: 8, 
        paddingVertical: 12,
        flexDirection: 'row',
        justifyContent: 'center',
        opacity: 0.6
            },   
            
    buttonG: {  
      width: '80%',
      backgroundColor:'#ea4235',
      borderRadius:7,
      marginVertical: 8, 
      paddingVertical: 12,
      flexDirection: 'row',
      justifyContent: 'center',
      opacity: 0.6
            },                   
     
      buttonTextF:{
        fontSize:14,
        fontWeight:'200',
        color:'#ffffff',
        textAlign: 'center',
        // paddingHorizontal: 40,
            },    
      
      buttonTextT:{
        fontSize:14,
        fontWeight:'200',
        color:'#ffffff',
        textAlign: 'center',
        // paddingHorizontal: 40,
        
            },
            
      buttonTextL:{
        fontSize:14,
        fontWeight:'200',
        color:'#ffffff',
        textAlign: 'center',
        // paddingHorizontal: 40,
        
            }, 
            
       buttonTextG:{
        fontSize:14,
        fontWeight:'200',
        color:'#ffffff',
        textAlign: 'center',
        // paddingHorizontal: 40,
        
            },     
            
       icon: {
        // paddingLeft: 20,
        // marginLeft:5
        alignSelf: 'flex-start',
        // justifyContent: 'flex-start'
       },     
            
    
      logo: {
        marginVertical: 30,
        width:width*0.5,
        height:height*0.4,
        marginHorizontal: '25%'
            },
            
      bg: {
        width:width*1, 
        height:height*1,
        
        // marginVertical: 10
      },

      buttonBox: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginBottom: 10,
      },


      signupButton: {
        color: '#fff',
        fontSize:10,
        marginTop: 20,
        marginLeft: 8,

     },

     logoSignup: {
       width:width*0.04,
       height:height*0.04,
       marginVertical: 16,
       marginLeft: 5
     },

     footer: {
       flex: 0.2,
       flexDirection: 'row',
       justifyContent: 'center',
      // marginBottom: 10,

     },

     buttonfooter: {
       flexDirection: 'row'
     },

     linelogo: {
      width:width*0.065,
      height:height*0.038,
     }
           
    });
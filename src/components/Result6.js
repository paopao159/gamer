import React from 'react';
import {StyleSheet, Text, View,TouchableHighlight,Platform,TouchableOpacity,ScrollView ,Image,TextInput,TouchableWithoutFeedback} from 'react-native';
import { Actions } from "react-native-router-flux";
// import { Icon } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';
import DismissKeyboard from "dismissKeyboard";


export default class Result6 extends React.Component {

  Result4() {
    Actions.result4()
  }
  Home() {
    Actions.home()
  }
  constructor(props){
    super(props)
    this.state = {
      text: "ส่งผลการแข่งขัน",
      colorBotton:'#2b7b4a',
      circleColorChange:0
    }
  }

  _handlePress = (team) => {
    // e.preventDefault();
    // styles.circleBtnBg.backgroundColor = 'transparent'
    this.setState({
      text: 'เสร็จสิ้น',
      colorBotton:'#2b7b4a',
      circleColorChange:team
    })
  }
  

  render() {
    const styles = StyleSheet.create({
      wrapper: { 
        flex: 1,
        paddingTop: Platform.os === "ios" ? 25 : 0,
        backgroundColor: '#cc6600',
       },
       result: {
        flexWrap: 'wrap',
        textAlign: 'center',
        fontSize: 35,
        marginTop: '10%',
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#FFFFFF',
       },
       tex0:{
         flexWrap: 'wrap',
         textAlign: 'center',
         fontSize: 20,
         fontWeight: '300',
         marginTop: 15,
         textAlign: 'center',
         color: '#FFFFFF',
       },
       tex2:{
        // marginTop: '10%',
        fontSize: 13,
        fontWeight: '300',
        textAlign: 'center',
        color: '#FFFFFF',
      },
      tex22:{
        marginTop: '10%',
        fontSize: 13,
        fontWeight: '300',
        textAlign: 'center',
        color: '#FFFFFF',
      },
      tex3:{
        textAlign: 'center',
        fontSize: 10,
        color: '#FFFFFF',
      },
       time:{
        marginTop: '20%',
        fontSize: 30,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#FFFFFF',
       },
       container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
      },
       containertex: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
       },
       circle1: {
        width: 100,
        height: 100,
        borderRadius: 100/2,
        alignItems: 'flex-start',
        marginTop: '25%',
        borderWidth: 3,
        borderColor: (this.state.circleColorChange == 1) ? 'green' :'#FFFFFF',
       },
       circle2: {
        width: 100,
        height: 100,
        borderRadius: 100/2,
        alignItems: 'flex-start',
        marginTop: '25%',
        borderWidth: 3,
        borderColor: (this.state.circleColorChange == 2) ? 'green' :'#FFFFFF',
       },
       button: {
        bottom: 0,
        position: 'absolute',
        marginVertical: 10,
        padding: 15,
        borderRadius:15, 
        width: 260,
        alignSelf:'center',
        backgroundColor: this.state.colorBotton,
       },
       buttontext: {
        fontSize: 17,
        width: '100%',
        textAlign: 'center',
        color: '#FFFFFF',
       },
       icon: {
        alignItems: 'flex-end',
        marginTop: 15,
       },
       circleBtnBg: {
         backgroundColor: 'transparent'
       },
       virt: {
        marginVertical: 10,
        // padding: 15,
        borderRadius:30, 
        width: 95,
        alignSelf:'center',
        backgroundColor: '#FFFFFF'
       },
       text40: {
        fontSize: 30,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#000000',
       },
       containertex1: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
       },
       box: {
        margin: 10,
        marginBottom: '25%',
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: 'flex-start',
       },
    });
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          DismissKeyboard();
        }}
        style={{ marginBottom: 0 }}
      >

    <View style={styles.wrapper}>
      
      <ScrollView>
          <Text style={styles.result}>
            ส่งผลการแข่งขัน
            </Text>
            <Text style={styles.tex0}>
            Screenshot ตอนจบเกม
            </Text>
        <View style={styles.container}>
        <View style={styles.containertex}>
          <TouchableHighlight underlayColor='#cc6600' onPress={()=>this._handlePress.bind(this)(1)} >
             <Image
                 source={require('../img/Virtuspro.png')}
                 style={styles.circle1}
              />
          </TouchableHighlight>
          <Text style={styles.tex22}>
              VirtusPro
          </Text>
          <View style={styles.virt}>
          <TextInput style={styles.text40}
              keyboardType='numeric'
          />
          </View>
        </View>
          <View style={styles.containertex}>
            <Text style={styles.tex2}>
            รอบที่: 1 
            </Text>
            <Text style={styles.tex3}> 
            19 พฤษภาคม 2018 
            </Text>
            <Text style={styles.time}>
            20:00
            </Text>
            </View>
            <View style={styles.containertex}>
            <TouchableHighlight underlayColor='#cc6600' onPress={()=>this._handlePress.bind(this)(2)}>
              <Image
                 source={require('../img/th.png')}
                 style={styles.circle2}
              />
            </TouchableHighlight>
            <Text style={styles.tex22}>
                   Thailand
            </Text>
            <View style={styles.virt}>
          <TextInput style={styles.text40}
          keyboardType='numeric'
          />
          </View>
        </View>
       </View>
       </ScrollView>

       <View style={styles.containertex1}>
            <Image
            source={require('../img/rov.png')}
            style={styles.box}
            />
        </View>

        <View style={styles.containertex}>
        
            <View style={styles.button}>
             <TouchableOpacity onPress={this.Result4}>
               <Text style={styles.buttontext}>
               {this.state.text}
               </Text>
              </TouchableOpacity>
            </View>
      </View>
    </View> 
    </TouchableWithoutFeedback>
    )
  }
}


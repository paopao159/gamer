import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  KeyboardAvoidingView,
  TextInput,
  Image,
  DeviceEventEmitter
} from "react-native";
import DismissKeyboard from "dismissKeyboard";
import { Actions } from "react-native-router-flux";
import Icon from 'react-native-vector-icons/FontAwesome';



const { width, height } = Dimensions.get("window");

export default class Login extends Component {
  constructor(props){
    super(props)
    this.state = {
      home:'login',
      loginBtnOpacity:1,
      signupOpacity:0.5
    };
    this.setHome = this.setHome.bind(this)
  }
  Home() {
    Actions.home()
  }
  LoginSocial() {
    Actions.loginSocial()
  }
  Home1() {
    Actions.home1()
  }

  updateIndex = index => {
    this.setState({ index });
  };

  setHome(page){
    
      this.setState({
        home:page
      })
      
  }

  render() {
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: "#e8e7e5",
        alignItems: "stretch"
      },
    
      logo: {
        width: width * 0.4,
        height: height * 0.3,
        position: 'absolute',
        marginVertical: 30,
        marginHorizontal: width* 0.3
      },
      bg: {
        width: width * 1,
        height: height * 1,
      },
    
      halfHeightLogin: {
        flex: 1,
        marginTop: 40,
        backgroundColor: "#e8e7e5",
        alignItems: "center",
        marginBottom: 0,
        display:(this.state.home == 'login') ? 'flex' : 'none'
      },
      halfHeightSingUp: {
        flex: 1,
        marginTop: 40,
        backgroundColor: "#e8e7e5",
        alignItems: "center",
        marginBottom: 0,
        display:(this.state.home == 'signup') ? 'flex' : 'none'
      },
    
      icon: {
        alignSelf: "flex-end",
        marginRight:100,
        marginTop: 40,
        position: "absolute",
        
      },
    
      inputWrap: {
        flex: 1,
        backgroundColor: "#e8e7e5",
        justifyContent: 'center'
      },

      inputWrapLogin: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#e8e7e5"
      },

      inputContainer: {
        borderWidth: 1,
        flexDirection: 'row',
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'transparent'
      },
    
      inputBox: {
        width: 320,
        backgroundColor: "#b4b4b4",
        borderRadius: 10,
        paddingHorizontal: 16,
        height: 45,
        fontSize: 16,
        color: "#2d3436",
        marginVertical: 8,
        textAlign: "center",
        fontWeight: "normal",
        
      },
    
      button: {
        width: 220,
        backgroundColor: "#105c8d",
        borderRadius: 10,
        paddingVertical: 14,
        paddingHorizontal: 18,
        height: 45,
        flexDirection: 'row',
        alignItems: 'center',
       
      },

      buttonSignup: {
        width: 200,
        backgroundColor: "#105c8d",
        borderRadius: 10,
        paddingVertical: 14,
        paddingHorizontal: 18,
        height: 45,
        flexDirection: 'row',
        alignItems: 'center',
        
      },
    
      buttonText: {
        fontSize: 16,
        fontWeight: "400",
        color: "#ffffff",
        textAlign: "center",
        marginLeft: '22%',
      },

      buttonTextSignup: {
        fontSize: 16,
        fontWeight: "400",
        color: "#ffffff",
        textAlign: "center",
        marginLeft: '15%',
      },

      buttonTextCancel:{
        fontSize: 16,
        fontWeight: "400",
        color: "#ffffff",
        textAlign: "center",
      },
    
      footerLogin: {
        flex: 0.5,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        borderTopWidth: 2,
        borderColor: "#d5d3d4",
        backgroundColor: "#e8e7e5",
        display:(this.state.home == 'login') ? 'flex' : 'none'
      },
      footerSignUp: {
        flex: 0.5,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        borderTopWidth: 2,
        borderColor: "#d5d3d4",
        backgroundColor: "#e8e7e5",
        display:(this.state.home == 'signup') ? 'flex' : 'none'
      },

      facebookButton: {
        color: "#999999",
        fontSize: 14,
        marginLeft: 10
      },

      buttonBox: {
        flexDirection: 'row'
      },

      buttonCancel: {
        width: 120,
        backgroundColor: "#f26060",
        borderRadius: 10,
        height: 45,
        marginLeft: 10,
        alignItems: 'center',
        justifyContent: 'center'
      },
    
      signupText: {
        color: "#cccac9",
        fontSize: 14
      },
    
      signupButton: {
        color: "#999999",
        fontSize: 14,
      },
    
      loginPage: {
        height: 40,
        width: width * 0.5,
        position: "absolute",
        backgroundColor: (this.state.home == 'login') ? 'black' : 'gray',
        opacity:  0.7,
        left: 0,
        alignItems: "center"
      },
    
      signupPage: {
        height: 40,
        width: width * 0.5,
        position: "absolute",
        right: 0,
        backgroundColor: (this.state.home == 'signup') ? 'black' : 'gray',
        opacity: 0.7,
        alignItems: "center"
      },
    
      pageText: {
        fontSize: 14,
        paddingVertical: 12,
        color: "white"
      },

      inputIcon: {
        width:width*0.08,
        height:height*0.05,
        resizeMode:'stretch',
        alignItems: 'center',
        position: 'absolute'
      },

      icon: {
        alignItems: 'flex-end',
        marginTop: 15,
       },

    });
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          DismissKeyboard();
        }}
        style={{ marginBottom: 0 }}
      >
      
        <View style={[styles.container, {height: this.state.visibleHeight}]}>
          <ImageBackground
            style={styles.bg}
            source={require("../img/bg.jpg")}
            resizeMode="cover"
            blurRadius={15}
          >
            <Image style={[styles.logo, this.state.topLogo]} source={require("../img/BW.png")} />
            <View style={styles.icon}>
            <Icon color="white" name="times-circle" size={30} onPress={this.LoginSocial} />
            </View>
    
            <View style={{ marginTop: "55%" }}>
              <TouchableOpacity onPress={() => this.setHome('signup')} style={styles.signupPage}>
                <Text style={styles.pageText}>ลงทะเบียน</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.setHome('login')} style={styles.loginPage}>
                <Text style={styles.pageText}>ล็อกอิน</Text>
              </TouchableOpacity>
            </View>
            
            
            <View style={styles.halfHeightLogin}>
                <View style={styles.inputWrapLogin}>
                  <TextInput
                    style={styles.inputBox}
                    underlineColorAndroid="rgba(0,0,0,0)"
                    placeholder="Email"
                    placeholderTextColor="#ffffff"
                    selectionColor="#ff9f43"
                    keyboardType="email-address"
                    onSubmitEditing={() => this.password.focus()}
                  />
                  <TextInput
                    style={styles.inputBox}
                    underlineColorAndroid="rgba(0,0,0,0)"
                    placeholder="Password"
                    secureTextEntry={true}
                    placeholderTextColor="#ffffff"
                    selectionColor="#ff9f43"
                    ref={input => (this.password = input)}
                  />
                </View>
            </View>
            
            
            <View style={styles.footerLogin}>
              <TouchableOpacity onPress={this.Home1} style={styles.button}>
                <Image style ={{width:width*0.055 ,height:height*0.055, marginLeft: 10 }}
                  source={require('../img/BW.png')}
                />
                <Text style={styles.buttonText}>ล็อกอิน</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.signup} style={{
                flexDirection: 'row',
                alignItems: 'center',
                flex:0.5,
                backgroundColor: '#e8e7e5',
                marginTop: 20,}}>
                <Image 
                    source={require('../img/icons8-forgot-password-26.png')} 
                 />
                <Text style={styles.signupButton}>ลืมรหัสผ่าน?</Text>
              </TouchableOpacity>
            </View>
            
            
            <View style={styles.halfHeightSingUp}>
            
              <KeyboardAvoidingView behavior="position" enable>
                
                <View style={styles.inputWrap}>
                  <View style={styles.inputContainer}>
                    <Image 
                    source={require('../img/icons8-user-filled-50.png')} 
                    style={styles.inputIcon} 
                    />
                  <TextInput
                    style={styles.inputBox}
                    underlineColorAndroid="rgba(0,0,0,0)"
                    placeholder="Username"
                    placeholderTextColor="#ffffff"
                    selectionColor="#ff9f43"
                    // keyboardType="email-address"
                    onSubmitEditing={() => this.displayname.focus()}
                  />
                 </View> 

                  <View style={styles.inputContainer}>
                  <Image 
                    source={require('../img/icons8-password-48.png')} 
                    style={styles.inputIcon} 
                    />
                   <TextInput
                    style={styles.inputBox}
                    underlineColorAndroid="rgba(0,0,0,0)"
                    placeholder="Display Name"
                    placeholderTextColor="#ffffff"
                    selectionColor="#ff9f43"
                    // keyboardType="email-address"
                    onSubmitEditing={() => this.password.focus()}
                    ref={(input) => this.displayname = input}
                  />
                  </View>

                   <View style={styles.inputContainer}>
                  <Image 
                    source={require('../img/icons8-password-48.png')} 
                    style={styles.inputIcon} 
                    />
                  <TextInput
                    style={styles.inputBox}
                    underlineColorAndroid="rgba(0,0,0,0)"
                    placeholder="Password"
                    secureTextEntry={true}
                    placeholderTextColor="#ffffff"
                    selectionColor="#ff9f43"
                    ref={input => (this.password = input)}
                  />
                </View>
                </View>
                
              </KeyboardAvoidingView>
              
            </View>
            
            <View style={styles.footerSignUp}>
              <View style={styles.buttonBox}>
              <TouchableOpacity onPress={this.home} style={styles.buttonSignup}>
              <Image style ={{width:width*0.055 ,height:height*0.055, marginLeft: 10 }}
                  source={require('../img/BW.png')}
                />
                <Text style={styles.buttonTextSignup}>สมัครสมาชิก</Text>
              </TouchableOpacity>
              
              <TouchableOpacity onPress={this.home} style={styles.buttonCancel}>
                <Text style={styles.buttonTextCancel}>ยกเลิก</Text>
              </TouchableOpacity>
              </View>
              <TouchableOpacity onPress={this.LoginSocial} style={{
                flexDirection: 'row',
                alignItems: 'center',
                flex:0.5,
                backgroundColor: '#e8e7e5',
                marginTop: 20,
                }}>
              <Icon style={styles.iconFacebook}
                name='facebook-square'
                type='font-awesome'
                color='gray'
                />
                <Text style={styles.facebookButton}>ล็อกอินด้วย Facebook</Text>
              </TouchableOpacity> 
            </View>
          </ImageBackground>
          
        </View>
        
      </TouchableWithoutFeedback>
    );
  } 
}
import React, { Component} from 'react';
import { StyleSheet, View ,Platform} from 'react-native';
import {Router, Stack, Scene} from 'react-native-router-flux'

import Login from './src/components/Login';
import LoginSocial from './src/components/LoginSocial';
import Home from './src/components/Home';
import Home1 from './src/components/Home1';
import Home2 from './src/components/Home2';
import Result from './src/components/Result';
import Result2 from './src/components/Result2';
import Result3 from './src/components/Result3';
import Result4 from './src/components/Result4';

import Result6 from './src/components/Result6';
import Chat from './src/components/Chat';
import Chathome from './src/components/Chathome';


export default class App extends Component {
  render() {
    return (
      <Router>
                <Stack key='root' hideNavBar={true} style={{paddingTop: Platform.OS === 'ios' ? 64 : 54}}>
                    <Scene key="login" component={Login} title="Login" />
                    <Scene key="loginSocial" component={LoginSocial} title="LoginSocial" />
                    <Scene key="home" component={Home} title="Home" />
                    <Scene key="home1" component={Home1} title="Home1" />
                    <Scene key="home2" component={Home2} title="Home2" />
                    <Scene key="result" component={Result} title="Result" />
                    <Scene key="result2" component={Result2} title="Result2" />
                    <Scene key="result3" component={Result3} title="Result3" />
                    <Scene key="result4" component={Result4} title="Result4" />

                    <Scene key="result6" component={Result6} title="Result6" />
                    <Scene key="chat" component={Chat} title="Chat" />
                    <Scene key="chathome" component={Chathome} title="Chathome" initial={true}/>


                </Stack>
                
            </Router>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  
});
// hideNavBar={true}
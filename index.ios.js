import {
    AppRegistry
} from 'react-native';

import App from './src/App';
import * as firebase from 'firebase';


AppRegistry.registerComponent('Chat', () => App);